import { createRequire } from 'module';
import sqlite3 from 'sqlite3';
import { open } from 'sqlite';

const require = createRequire(import.meta.url);

const TelegramBot = require('node-telegram-bot-api');

export { };

const channelId = 000000000;
const botToken = '';
const dbLocation = '/location/of/you-database.db';

const bot = new TelegramBot(botToken);

const db = await open({
  filename: dbLocation,
  driver: sqlite3.Database,
});

const userSet = new Set((await db.all('SELECT distinct username FROM v1_config_current_players ORDER BY username ASC')).map((u) => u.username));

async function poll() {
  try {
    const users = (await db.all('SELECT distinct username FROM v1_config_current_players ORDER BY username ASC')).map((u) => u.username);
    const joined = users.filter((u) => !userSet.has(u));
    const left = Array.from(userSet.values()).filter((u) => !users.includes(u));
    if (joined.length > 0 || left.length > 0) {
      const joinedMessage = `${joined.join(', ')} ${joined.length != 1 ? 'conectaron' : 'conectó'}`;
      const leftMessage = `${left.join(', ')} ${left.length != 1 ? 'salieron' : 'salió'}`;
      const statusMessage = `Hay ${users.length} ${users.length != 1 ? 'jugadores' : 'jugador'} en línea${users.length > 0 ? ': ' + users.join(', ') : ''}`;
      const message = `${joined.length > 0 ? `${joinedMessage}\n` : ''}${left.length > 0 ? `${leftMessage}\n` : ''}${statusMessage}`;
      bot.sendMessage(channelId, message);
      joined.forEach((u) => userSet.add(u));
      left.forEach(u => userSet.delete(u));
    }
  } catch (err) {
    console.log(err);
  }
  setTimeout(poll, 5000);
}

poll();
