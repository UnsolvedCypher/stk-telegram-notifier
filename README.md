# Español

Este bot manda un mesaje a un canal en Telegram cuando alguien conecta o disconecta de un servidor en SuperTuxKart. Necesita `NodeJS` y `yarn`.

Para configurarlo:

`git clone https://gitlab.com/UnsolvedCypher/stk-telegram-notifier/`

`cd stk-telegram-notifier`

`yarn install`

En `main.js`, cambia el botToken al token de su bot, channelId al canal a que el bot debe enviar mensajes, y dbLocation a la ubicacion del archivo .db que contiene los records, usuarios, y otra informacion.

`yarn start`